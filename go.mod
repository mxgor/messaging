module gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging

go 1.21.3

toolchain go1.21.5

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.2.0 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/lestrrat-go/blackmagic v1.0.2 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.4 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/jwx/v2 v2.0.16 // indirect
	github.com/lestrrat-go/option v1.0.1 // indirect
	github.com/segmentio/asm v1.2.0 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core v0.0.0-20231109072850-c268bd1b9cee // indirect
	gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/jwt v0.0.0-20231109152304-f2d1d3229022 // indirect
	gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip v0.0.5 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/sys v0.14.0 // indirect
)
